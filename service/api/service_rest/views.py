from django.http import JsonResponse
from common.json import ModelEncoder
from service_rest.models import Technician, Appointment, AutomobileV0
import json
from django.views.decorators.http import require_http_methods


class AutomobileV0Encoder(ModelEncoder):
    model = AutomobileV0
    properties = [
        "color",
        "year",
        "vin", ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_no",
        "id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "appointment_time",
        "reason",
        "status",
        "customer_name",
        "vin",
        "vip_customer",
        "technician",
        "id"
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }


@require_http_methods(["POST"])
def create_technician(request):
    if request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        return JsonResponse(
                {"message": "Invalid Method"},
                status=400,
        )


@require_http_methods(["POST"])
def create_service_appointment(request):
    if request.method == "POST":
        content = json.loads(request.body)
        inventory = AutomobileV0.objects.filter(vin=content["vin"])
        is_vip = False
        if inventory:
            is_vip = True
        appointment = Appointment(
            appointment_time=content["appointment_time"],
            reason=content["reason"],
            customer_name=content["customer_name"],
            vin=content["vin"],
            vip_customer=is_vip,
            technician=Technician.objects.get(id=content["technician"])
        )
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        return JsonResponse(
                {"message": "Invalid Method"},
                status=400,
                )


@require_http_methods(["GET"])
def show_service_history(request, vin):
    if request.method == "GET":
        appointments = Appointment.objects.filter(vin=vin)

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        return JsonResponse(
                {"message": "Invalid Method"},
                status=400, )


@require_http_methods(["GET"])
def list_service_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.filter(status='PD')
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        return JsonResponse(
            {"message": "Invalid Method"},
            status=400,
            )


@require_http_methods(["PUT"])
def update_appointment(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        content = json.loads(request.body)
        if "status" in content:
            appointment.status = content["status"]
            appointment.save()
        return JsonResponse({"appointment": appointment},
                            encoder=AppointmentEncoder,
                            status=200,
                            )
    else:
        return JsonResponse({"message": "Method not allowed"},
                            status=400)


@require_http_methods(["GET"])
def list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()

    return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
