from django.contrib import admin

# Register your models here.
from .models import Appointment, Technician


admin.site.register(Appointment)
admin.site.register(Technician)
