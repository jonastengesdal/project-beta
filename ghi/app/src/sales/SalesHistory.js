import { useEffect, useState } from "react";


function SalesHistory() {
    const [sales_people, setSalesPeople] = useState([]);
    const [saleList, setSaleList] = useState([]);

    const [employee_number, setEmployeeNum] = useState('');

    const handleEmployeeNumChange = (event) => {
        const value = event.target.value;
        setEmployeeNum(value);
    }

    const fetchSalesPeopleData = async () => {
        const url = 'http://localhost:8090/api/sales_people/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.sales_people)
        }
    }

    useEffect(() => {
        fetchSalesPeopleData();
    }, []);

    async function handleSelect() {
        const response = await fetch('http://localhost:8090/api/sale_lists/'+employee_number+'/')
        if(response.ok) {
            const data = await response.json();
            setSaleList(data.sale_lists);
        }
        else {
            console.error(response);
        }
    }


return (
    <div>
        <br />
        <div className="mb-3">
        <h1>Sales person history</h1>
        <form onSelect={handleSelect} id="view-sales-history">

        <select onChange={handleEmployeeNumChange} value={employee_number} required id="employee_number" name="employee_number" className="form-select">
            <option value="">Choose an employee number</option>
            {sales_people.map(sales_people => {
                return (
                    <option key={sales_people.id} value={sales_people.id}>
                        {sales_people.employee_number}
                    </option>
                );
            })}
        </select>
        </form>
        </div>
        <br />
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Sales Person Name</th>
                <th>Customer Name</th>
                <th>Automobile VIN</th>
                <th>Sale Price</th>
            </tr>
            </thead>
            <tbody>
                {saleList.length ?
                    saleList.map(sale_lists =>
                        <tr key={ sale_lists.record.sales_person_id }>
                            <td>{ sale_lists.record.sales_person.name }</td>
                            <td>{ sale_lists.record.customer.name }</td>
                            <td>{ sale_lists.record.automobile.vin }</td>
                            <td>{ sale_lists.record.price }</td>
                        </tr>
                    )
                    : <tr></tr>
                }
            </tbody>
        </table>
    </div>
    );
}

export default SalesHistory;
