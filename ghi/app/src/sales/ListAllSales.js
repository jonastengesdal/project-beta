import { useEffect, useState } from "react";
import { Link } from "react-router-dom";


function ListAllSales() {
    const [saleList, setSaleList] = useState([]);
    useEffect(() => {
        loadSaleList();
    }, []);

    async function loadSaleList() {
        const response = await fetch('http://localhost:8090/api/sale_lists/');
        if(response.ok) {
            const data = await response.json();
            setSaleList(data.sale_lists);
        }
        else {
            console.error(response);
        }
    }

    return (
        <div>
            <br />
            <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
                <Link to='/sale_lists/new' className="btn btn-primary btn-lg px-4 gp-3">Sale Records</Link>
            </div>
            <br />
            <h2>Sale Records</h2>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Sales Person Name</th>
                    <th>Sales Person Employee Number</th>
                    <th>Purchaser Name</th>
                    <th>Automobile VIN</th>
                    <th>Sale Price</th>
                </tr>
                </thead>
                <tbody>
                {saleList.map(sale_lists => {
                    return (
                    <tr key={sale_lists.id}>
                        <td>{ sale_lists.record.sales_person.name }</td>
                        <td>{ sale_lists.record.sales_person.employee_number }</td>
                        <td>{ sale_lists.record.customer.name }</td>
                        <td>{ sale_lists.record.automobile.vin }</td>
                        <td>{ sale_lists.record.price }</td>
                    </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
    );
}

export default ListAllSales;
