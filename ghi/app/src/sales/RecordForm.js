import React, { useState, useEffect } from 'react';


function RecordForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [sales_people, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [automobile, setAutomobile] = useState('');
    const [sales_person, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const fetchAutomobilesData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
            }
        }
    const fetchCustomersData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }
    const fetchSalesPeopleData = async () => {
        const url = 'http://localhost:8090/api/sales_people/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.sales_people)
        }
    }

    useEffect(() => {
        fetchAutomobilesData();
        fetchCustomersData();
        fetchSalesPeopleData();

    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile;
        data.sales_person = sales_person;
        data.customer = customer;
        data.price = price;
        const recordsUrl = 'http://localhost:8090/api/records/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(recordsUrl, fetchOptions)
        if (response.ok) {
            setSalesPerson('');
            setCustomer('');
            setPrice('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a sale record</h1>
            <form onSubmit={handleSubmit} id="create-record-form">
                <div className="mb-3">
                <select onChange={handleAutomobileChange} value={automobile} required id="automobile" name="automobile" className="form-select">
                    <option value="">Choose an automobile</option>
                    {automobiles.map(autos => {
                        return (
                            <option key={autos.id} value={autos.vin}>
                                {autos.model.manufacturer.name}  {autos.model.name}
                            </option>
                        );
                    })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleSalesPersonChange} value={sales_person} required id="sales_person" name="sales_person" className="form-select">
                    <option value="">Choose a sales person</option>
                    {sales_people.map(sales_person => {
                        return (
                            <option key={sales_person.name} value={sales_person.name}>
                                {sales_person.name}
                            </option>
                        );
                    })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                    <option value="">Choose a customer</option>
                    {customers.map(customer => {
                        return (
                            <option key={customer.name} value={customer.name}>
                                {customer.name}
                            </option>
                        );
                    })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <input data-type="currency" onChange={handlePriceChange} value={price} placeholder="Sale Price" required type="number" name="price" id="price" className="form-control"/>
                <label htmlFor="currency-field">Sale Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default RecordForm;
