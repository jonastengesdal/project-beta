import { React, useEffect, useState } from "react";

function AppointmentForm() {
    const [vin, setVin] = useState('');
    const handlevinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const [customer_name, setCustomer_name] = useState('');
    const handlecustomer_nameChange = (event) => {
        const value = event.target.value;
        setCustomer_name(value);
    }
    const [appointment_date, setAppointment_date] = useState('');
    const handleappointment_dateChange = (event) => {
        const value = event.target.value;
        setAppointment_date(value);
    }
    const [appointment_time, setAppointment_time] = useState('');
    const handleappointment_timeChange = (event) => {
        const value = event.target.value;
        setAppointment_time(value);
    }
    const [technician, setTechnician] = useState('');
    const handletechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const [technicians, setTechnicians] = useState([]);

    const [reason, setReason] = useState('');
    const handlereasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.vin = vin;
        data.customer_name = customer_name;
        data.appointment_time = appointment_date+'T'+appointment_time;
        data.technician = technician;
        data.reason = reason;
        console.log(data);

        const appointmentUrl = `http://localhost:8080/service/api/appointment/create`;
        const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                "Content-Type": "application/json",
                },
            };
        const response = await fetch(appointmentUrl, fetchConfig, {mode: 'no-cors'});
        if (response.ok) {
            setVin('');
            setCustomer_name('');
            setAppointment_time('');
            setTechnician('');
            setReason('');
        }
    }
    // fetchData, useEffect, and return methods...
    const fetchData = async () => {
        const url = 'http://localhost:8080/service/api/technician/list';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
            //console.log(bins);
            //console.log(fetchData);
        }
    }
    useEffect(() => {
        fetchData();
        }, [] );

        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new appointmentform</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input onChange={handlevinChange} placeholder="vin" required type="text" name="vin" value={vin} id="vin" className="form-control"/>
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlecustomer_nameChange} placeholder="customer_name" required type="text" name="customer_name" value={customer_name} id="customer_name" className="form-control"/>
                        <label htmlFor="customer_name">Customer Name</label>
                    </div>
                    <div className="mb-3">
                        <input onChange={handleappointment_dateChange} placeholder="appointment_date" required type="date" name="appointment_date" id="appointment_date" value={appointment_date} className="form-control"/>
                        <label htmlFor="appointment_time">Appointment Date</label>
                    </div>
                    <div className="mb-3">
                        <input onChange={handleappointment_timeChange} placeholder="appointment_time" required type="time" name="appointment_time" id="appointment_time" value={appointment_time} className="form-control"/>
                        <label htmlFor="appointment_time">Appointment Time</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlereasonChange} placeholder="reason" required type="text" name="reason" id="reason" value={reason} className="form-control"/>
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handletechnicianChange} required name="technician" id="technician" value={technician} className="form-select">
                            <option value="">Choose a technician</option>
                            {technicians.map(technician => {
                                return (
                                <option key={technician.id} value={technician.id}>{technician.name}</option>
                            );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create an appointment</button>
                </form>
                </div>
                </div>
            </div>
                );
                }

export default AppointmentForm;
