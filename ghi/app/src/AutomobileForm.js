import { React, useEffect, useState } from "react";

function AutomobileForm() {
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [year, setYear] = useState([]);
    const handleyearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }
    const [vin, setVin] = useState([]);
    const handlevinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const [model, setModel] = useState([]);
    const handlemodelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }
    const [vehichleModels, setVehicleModels] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const Url = `http://localhost:8100/api/automobiles/`;
        const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                "Content-Type": "application/json",
                },
            };
        const response = await fetch(Url, fetchConfig, {mode: 'no-cors'});
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel('');
        }
    }
    // fetchData, useEffect, and return methods...
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models);
        }
    }
    useEffect(() => {
        fetchData();
        }, [] );

        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Add an Automobile to inventory</h1>
                <form onSubmit={handleSubmit} id="create-automobiles-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} placeholder="color" required type="text" name="color" value={color} id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleyearChange} placeholder="year" required type="text" name="year" value={year} id="year" className="form-control"/>
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlevinChange} placeholder="vin" required type="text" name="vin" value={vin} id="vin" className="form-control"/>
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                    <select onChange={handlemodelChange} required name="model" id="model" value={model} className="form-select">
                    <option value="">Choose a model</option>
                    {vehichleModels.map(vehichleModel => {
                        return (
                        <option key={vehichleModel.id} value={vehichleModel.id}>{vehichleModel.name}</option>
                    );
                    })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create an Automobile</button>
                </form>
                </div>
                </div>
            </div>
                );
                }

export default AutomobileForm;
