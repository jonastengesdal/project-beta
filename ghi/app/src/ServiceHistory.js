import { useEffect, useState } from "react";
function ServiceHistory() {
    const [vin, setVin] = useState([]);
    const handlevinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const [appointmentList, setAppointmentList] = useState([]);

    useEffect(() => {
    }, []);

    async function handleSearch() {
        const response = await fetch('http://localhost:8080/service/api/vin/'+vin+'/list');
        if(response.ok) {
            const data = await response.json();
            setAppointmentList(data.appointments);
        }
        else {
            console.error(response);
        }
    }

return (
    <div>
        <br />
        <div className="mb-3 justify-content-sm-center">
            <input onChange={handlevinChange} placeholder="vin" required type="text" name="vin" value={vin} id="vin" className="form-control"/>
            <button onClick={() => handleSearch()} className="btn btn-primary btn-lg px-4 gp-3">Search VIN</button>
        </div>
        <br />
    <table className="table table-striped">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Reason</th>
        </tr>
        </thead>
        <tbody>
            {appointmentList.length ?
                appointmentList.map(data =>
                    <tr key={ data.vin }>
                        <td>{ data.vin }</td>
                        <td>{ data.customer_name }</td>
                        <td>{ data.appointment_time }</td>
                        <td>{ data.technician.name }</td>
                        <td>{ data.reason }</td>
                    </tr>
                )
                : <tr></tr>
            }
        </tbody>
    </table>
    </div>
    );
}
export default ServiceHistory;
