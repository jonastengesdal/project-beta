import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);
    useEffect(() => {
        loadAutomobile();
    }, []);

    async function loadAutomobile() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if(response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
        else {
            console.error(response);
        }
    }
return (
    <div>
        <br />
        <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
            <Link to='/inventory/automobiles/new' className="btn btn-primary btn-lg px-4 gp-3">Create an Automobile</Link>
        </div>
        <br />
        <h2>Inventory Vehicles</h2>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
        </tr>
        </thead>
        <tbody>
            {automobiles.map(auto =>
                <tr key={ auto.id }>
                    <td>{ auto.vin }</td>
                    <td>{ auto.color }</td>
                    <td>{ auto.year }</td>
                    <td>{ auto.model.name }</td>
                    <td>{ auto.model.manufacturer.name }</td>
                </tr>
            ) }
        </tbody>
    </table>
    </div>
    );
}
export default AutomobileList;
