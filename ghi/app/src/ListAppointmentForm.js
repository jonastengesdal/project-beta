import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function ListappointmentForm() {
    const [appointmentList, setAppointmentList] = useState([]);
    useEffect(() => {
        loadappointment();
    }, []);

    async function cancelAppointment(id)
    {
        const data = {
            status: 'CL'
        }
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data)
        };
        const response = await fetch('http://localhost:8080/service/api/appointment/'+id+'/update', fetchConfig);
        if(response.ok) {
            loadappointment();
        }
    }

    async function finishAppointment(id)
    {
        const data = {
            status: 'FN'
        }
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data)
        };
        const response = await fetch('http://localhost:8080/service/api/appointment/'+id+'/update', fetchConfig);
        if(response.ok) {
            loadappointment();
        }
    }

    async function loadappointment() {
        const response = await fetch('http://localhost:8080/service/api/');
        if(response.ok) {
            const data = await response.json();
            setAppointmentList(data.appointments);
        }
        else {
            console.error(response);
        }
    }
return (
    <div>
        <br />
        <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
            <Link to='/service/appointments/new' className="btn btn-primary btn-lg px-4 gp-3">Create an Appointment</Link>
            <Link to='/service/technician/new' className="btn btn-primary btn-lg px-4 gp-3">Create a Technician</Link>
            <Link to='/service/appointments/history' className="btn btn-primary btn-lg px-4 gp-3">Lookup Service History</Link>
        </div>
        <br />
        <h2>Service Appointments</h2>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Reason</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            {appointmentList.map(data =>
                <tr key={ data.vin }>
                    <td>{ data.vin }</td>
                    <td>{ data.customer_name }</td>
                    <td>{ data.appointment_time }</td>
                    <td>{ data.technician.name }</td>
                    <td>{ data.reason }</td>
                    <td><button onClick={() => cancelAppointment(data.id)}>Cancel</button>
                    <button onClick={() => finishAppointment(data.id)}>Finished</button></td>
                </tr>
            ) }
        </tbody>
    </table>
    </div>
    );
}
export default ListappointmentForm;
