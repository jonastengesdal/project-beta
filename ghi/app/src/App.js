import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage.js';
import Nav from './Nav.js';
import ListAppointmentForm from './ListAppointmentForm.js';
import AppointmentForm from './AppointmentForm.js';
import TechnicianForm from './TechnicianForm.js';
import ServiceHistory from './ServiceHistory.js';
import VehicleModelsList from './VehicleModelsList.js';
import VehicleModelsForm from './VehicleModels.js';
import ManufacturerForm from './ManufacturerForm.js';
import ListManufacturer from './ListManufacturers.js';
import AutomobileList from './AutomobileList.js';
import AutomobileForm from './AutomobileForm.js';
import SalesPersonForm from './sales/SalesPersonForm';
import SalesPersonList from './sales/SalesPersonList';
import CustomerForm from './sales/CustomerForm';
import CustomerList from './sales/CustomerList';
import RecordForm from './sales/RecordForm';
import RecordList from './sales/RecordList';
import ListAllSales from './sales/ListAllSales';
import SalesHistory from './sales/SalesHistory';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="service/appointments/" element={<ListAppointmentForm />} />
          <Route path="service/appointments/new" element={<AppointmentForm/>}/>
          <Route path="service/technician/new" element={<TechnicianForm />} />
          <Route path="service/appointments/history" element={<ServiceHistory />} />
          <Route path="inventory/models/" element={<VehicleModelsList />}/>
          <Route path="inventory/models/new" element={<VehicleModelsForm />}/>
          <Route path="inventory/manufacturers/" element={<ListManufacturer />}/>
          <Route path="inventory/manufacturers/new" element={<ManufacturerForm />}/>
          <Route path="inventory/automobiles/" element={<AutomobileList />}/>
          <Route path="inventory/automobiles/new" element={<AutomobileForm />}/>
          <Route path="sales_people/new" element={<SalesPersonForm />} />
          <Route path="sales_people/" element={<SalesPersonList />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="customers/" element={<CustomerList />} />
          <Route path="records/new" element={<RecordForm />} />
          <Route path="records/" element={<RecordList />} />
          <Route path="sale_lists/" element={<ListAllSales />} />
          <Route path="sale_lists/history" element={<SalesHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
