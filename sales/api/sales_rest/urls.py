from django.urls import path
from .views import (
    api_list_sales_people,
    api_show_sales_person,
    api_list_customers,
    api_show_customer,
    api_list_records,
    api_show_record,
    api_list_sales,
    show_sales_history,
)

urlpatterns = [
    path("sales_people/", api_list_sales_people, name="api_list_sales_people"),
    path("sales_people/<int:pk>/", api_show_sales_person, name="api_show_sales_person"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("records/", api_list_records, name="api_list_records"),
    path("records/<int:pk>/", api_show_record, name="api_show_record"),
    path("sale_lists/", api_list_sales, name="api_list_sales"),
    path("sale_lists/<int:pk>/", show_sales_history, name="show_sales_history"),
]
